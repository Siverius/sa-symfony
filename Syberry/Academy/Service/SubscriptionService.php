<?php

namespace Syberry\Academy\Service;

use Syberry\Academy\Infrastructure\Subscription\SubscriptionGatewayApi;
use Syberry\Academy\Infrastructure\Subscription\SubscriptionRepository;

class SubscriptionService
{
    /**
     * @var SubscriptionRepository
     */
    private $subscriptionRepository;

    /**
     * @var SubscriptionGatewayApi
     */
    private $subscriptionGatewayApi;

    /**
     * SubscriptionService constructor.
     */
    public function __construct()
    {
        $this->subscriptionRepository = new SubscriptionRepository();
        $this->subscriptionGatewayApi = new SubscriptionGatewayApi();
    }


    public function cancelSubscription($user)
    {
        $activeSubscription = $this->subscriptionRepository->getActiveSubscription($user);
        $this->subscriptionGatewayApi->cancel($activeSubscription->getGatewayId());
    }
}