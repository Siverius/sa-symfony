<?php

namespace Syberry\Academy\Data\Subscription;

use App\Data\Model;

class Subscription extends Model
{
    /**
     * @var SubscriptionPlan
     */
    private $plan;

    /**
     * Subscription constructor.
     *
     * @param int $id
     */
    public function __construct($id)
    {
        parent::__construct($id);
        $this->plan = new SubscriptionPlan($id);
    }

    /**
     * @return SubscriptionPlan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @return int
     */
    public function getGatewayId()
    {
        return $this->getId();
    }
}
