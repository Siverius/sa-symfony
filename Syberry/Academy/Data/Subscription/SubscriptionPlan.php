<?php

namespace Syberry\Academy\Data\Subscription;

use App\Data\Model;

class SubscriptionPlan extends Model
{
    const FREE = 1;
    const PAID = 2;
}
