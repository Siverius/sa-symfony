<?php

namespace Syberry\Academy\Exceptions;

class CanNotCancelSubscriptionException extends \Exception
{
    public $code = 422;
    public $message = 'The subscription wasn\'t cancelled due to technical issue';
}
