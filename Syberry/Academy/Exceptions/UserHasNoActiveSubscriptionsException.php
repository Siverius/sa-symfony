<?php

namespace Syberry\Academy\Exceptions;

class UserHasNoActiveSubscriptionsException extends \Exception
{
    public $code = 404;
    public $message = 'Subscription is not found for user';
}
