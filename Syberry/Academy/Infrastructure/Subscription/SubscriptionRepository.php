<?php

namespace Syberry\Academy\Infrastructure\Subscription;

use Syberry\Academy\Data\Subscription\Subscription;
use Syberry\Academy\Data\User;
use Syberry\Academy\Exceptions\UserHasNoActiveSubscriptionsException;

class SubscriptionRepository
{
    public function getActiveSubscription(User $user)
    {
        // condition in order to emulate error
        if ($user->getId() % 10 == 0) {
            throw new UserHasNoActiveSubscriptionsException();
        }
        return new Subscription($user->getId());
    }
}