<?php

use Syberry\Academy\Controller\SubscriptionController;
use Syberry\Academy\Exceptions\UserIdMissedException;

require_once('autoload.php');

if (!isset($argv[1])) {
    throw new UserIdMissedException();
}

try {
    list($key, $val) = explode('=', $argv[1]);
} catch (UserIdMissedException $e) {
    die($e->message);
}

if (strtolower($key) !== 'userid') {
    die('UserId needed');
}

$sc = new SubscriptionController();
$sc->cancelSubscriptionForUser($val);