<?php

namespace Syberry\Academy\Infrastructure\Subscription;

use Syberry\Academy\Exceptions\CanNotCancelSubscriptionException;

class SubscriptionGatewayApi
{

    /**
     * @param $id
     * @throws CanNotCancelSubscriptionException
     */
    public function cancel($id)
    {
        // emulates error
        if ($id % 101 == 0) {
            throw new CanNotCancelSubscriptionException();
        }
    }
}