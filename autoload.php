<?php

spl_autoload_register(
    function ($class) {
        $class = str_replace('App\\Data', 'vendor/model/', $class);
        $file = str_replace('\\', '/', $class) . '.php';
        if (is_file($file)) {
            require_once($file);
        }
    }
);