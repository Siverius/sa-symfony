<?php


namespace Syberry\Academy\Exceptions;


class UserIdMissedException extends \Exception
{
    public $message = 'UserId missed';
}