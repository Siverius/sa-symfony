<?php

namespace Syberry\Academy\Controller;

use Syberry\Academy\Data\User;
use Syberry\Academy\Exceptions\CanNotCancelSubscriptionException;
use Syberry\Academy\Exceptions\UserHasNoActiveSubscriptionsException;
use Syberry\Academy\Infrastructure\Http\Response;
use Syberry\Academy\Service\SubscriptionService;

class SubscriptionController
{
    private $subscriptionService;

    /**
     * SubscriptionController constructor.
     */
    public function __construct()
    {
        $this->subscriptionService = new SubscriptionService();
    }


    public function cancelSubscriptionForUser($userId)
    {
        $user = new User($userId);

        try {
            if($this->subscriptionService->cancelSubscription($user)) {
                return null;
            }
        } catch (UserHasNoActiveSubscriptionsException $e) {
            http_response_code($e->code);
            return new Response($e->code, $e->message);
        } catch (CanNotCancelSubscriptionException $e) {
            http_response_code($e->code);
            return new Response($e->code,$e->message);
        }

        return new Response(200);
    }
}